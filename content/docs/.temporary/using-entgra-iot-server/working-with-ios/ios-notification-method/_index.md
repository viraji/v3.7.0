# iOS Notification Method

Entgra IoT Server (Entgra IoTS) only uses the Apple Push Notification Service (APNS) notification method for devices that use the iOS mobile OS. 

## APNS

The APNS notification method uses the APNS server to wake-up the iOS device. When there are pending operations that need to be enforced on a device, Entgra IoTS will send a push notification to the APNS server and the APNS server will in-turn send the message to the respective iOS client that is within the iOS device. When the iOS client receives the push notification, it will communicate with Entgra IoTS and receive the list of pending operations that need to be enforced on the iOS device.

 ![image](352824347.png)





For more information on setting the iOS configurations, see the [iOS Platform Configurations](/doc/en/lb2/iOS-Platform-Configurations.html).




---
bookCollapseSection: true
weight: 8
---
# Working with iOS Devices

The following sections describe the initial configurations that the system administrator needs to carry out in order to be able to work with iOS device types on Entgra IoT Server.

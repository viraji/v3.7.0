---
bookCollapseSection: true
weight: 2
---
# About this Release

The current version of Entgra IoT released is 3.7.0.

## What's New in This Release

[Entgra IoT Server](https://entgra.io/) version 3.7.0 is the successor of [Entgra IoT Server 3.6.0](https://entgra.atlassian.net/wiki/spaces/IoTS360/overview). Some prominent features and enhancements are as follows:

*   [Device Enrollment Program (DEP) for iOS devices](https://entgra-documentation.gitlab.io/v3.7.0/docs/using-entgra-iot-server/working-with-ios/device-enrollment-program/).

*   GDPR Compliance - Making Entgra IoT Server 3.3.0 compliant with GDPR
* Scrap User Information - [Removing user and device details when the user requests to be forgotten](https://entgra-documentation.gitlab.io/v3.7.0/docs/using-entgra-iot-server/product-administration/General-Data-Protection-Regulation-for-WSO2-IoT-Server/).

* [Consent Management within Entgra Server](https://entgra-documentation.gitlab.io/v3.7.0/docs/using-entgra-iot-server/installation-guide/installing-the-product/)

*   [Consent Management at Device Enrollment](https://entgra-documentation.gitlab.io/v3.7.0/docs/tutorials/android/)

*   Cookie and Privacy Policy Management at Device Management Console

* [Android] - Optional Data Backup - Ability to Turn on and Off Data Backing Up

* [Android] - Automated Enrollment Using QR Code

* [Android] [iOS] - Adding  Application Configuration - New operation to add application configuration in Android/iOS.

* [iOS] - New Policy for Installing Fonts

* [iOS] - Voice Roaming Control - Ability to control voice roaming specific to country, package etc.

* [iOS]  - Data Roaming Control - Ability to control data roaming as per package, country etc.

* [iOS] - WiFi Hotspot Control - Ability to control wifi hotspot

* [iOS] - Bluetooth Control - Ability to control bluetooth

* [iOS] - Wallpaper Options - Ability to change wallpaper

* [iOS] - Install MacOs Applications

* [iOS] New Policy for Network Usage Rules 

* [iOS] Active Sync Integration


## Compatible versions

Entgra IoT Server is compatible with WSO2 Carbon 4.4.17 products. For more information on the products in each Carbon platform release, see the [Release Matrix](http://wso2.com/products/carbon/release-matrix/).

## Known issues

For a list of known issues in this release, see [Entgra IoT Server 3.4.0 - Known Issues](https://gitlab.com/entgra/product-iots/).

## Fixed issues

For a list of fixed issues in this release, see [Entgra IoT Server 3.4.0 - Fixed Issues](https://gitlab.com/entgra/product-iots/issues?scope=all&utf8=%E2%9C%93&state=closed).
